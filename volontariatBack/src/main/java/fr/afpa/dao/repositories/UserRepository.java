package fr.afpa.dao.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.entitedao.UserDao;

public interface UserRepository extends JpaRepository<UserDao, Long>{

	public UserDao findByUserDaoName(String username);
}
