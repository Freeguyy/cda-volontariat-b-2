package fr.afpa.dao.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.entitedao.PersonneDao;

public interface UtilisateurRepository extends JpaRepository <PersonneDao, Long> {

	List<PersonneDao> findByRoleIdRole(Long id);
}
