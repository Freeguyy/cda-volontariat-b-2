package fr.afpa.dao.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.entitedao.RoleDao;

public interface RoleRepository extends JpaRepository<RoleDao,Long> {
	
}


