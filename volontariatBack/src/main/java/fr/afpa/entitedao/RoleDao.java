package fr.afpa.entitedao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString

@Entity
@Table(name  = "roleprofil")
public class RoleDao {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idRole;
	
	@Column
	private String titreRole;
	
	@OneToMany(mappedBy = "role")
	private List<PersonneDao> personne;
	
	public RoleDao() {
		super();
	}
	
	public RoleDao(String titreRole) {
		this.titreRole = titreRole;
		personne = new ArrayList<PersonneDao>();
	}
}
