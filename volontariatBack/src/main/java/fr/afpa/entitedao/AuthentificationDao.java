package fr.afpa.entitedao;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString

@Entity
@Table(name="authentification")
public class AuthentificationDao {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "authentification_generator")
	@SequenceGenerator(name = "authentification_generator", sequenceName = "authentification_seq",allocationSize = 1, initialValue = 2)
	@Column (name="idAuthentification", updatable = false, nullable = false )
	private Long idAuthentification;
	
	@Column
	private String login;
	
	@Column
	private String password;
	
	@JsonIgnore
	@OneToOne(cascade = { CascadeType.MERGE })
	@JoinColumn(name = "idPersonne")
	private PersonneDao personne;

	public AuthentificationDao() {
		super();
	}
	
	public AuthentificationDao(String login, String password) {
		this.login = login;
		this.password = password;
	}

}
