package fr.afpa.dto.services;

import java.util.ArrayList;
import java.util.List;

import fr.afpa.entitedao.AuthentificationDao;
import fr.afpa.entitedao.PersonneDao;
import fr.afpa.entitedao.RoleDao;
import fr.afpa.entitemetier.Admin;
import fr.afpa.entitemetier.Authentification;
import fr.afpa.entitemetier.Demandeur;
import fr.afpa.entitemetier.Personne;
import fr.afpa.entitemetier.Role;
import fr.afpa.entitemetier.Volontaire;

public class ServicesMappingDto {

public static AuthentificationDao mappingAuthentificationMetierToAuthentificationDAO(Authentification authentification) {
		
		AuthentificationDao authentificationDao = new AuthentificationDao();
		authentificationDao.setIdAuthentification(authentification.getIdAuthentification());
		authentificationDao.setLogin(authentification.getLogin());
		authentificationDao.setPassword(authentification.getMotDePasse());
		//authentificationDao.setPersonne(mappingPersonneMetierToPersonneDao(authentification.getPersonne()));
		return authentificationDao;
	}

	public static  PersonneDao mappingPersonneMetierToPersonneDao(Personne personneMetier) {
		PersonneDao personneDao = new PersonneDao();
		personneDao.setIdPersonne(personneMetier.getId());
		personneDao.setNom(personneMetier.getNom());
		personneDao.setPrenom(personneMetier.getPrenom());
		personneDao.setMail(personneMetier.getEmail());
		personneDao.setTel(personneMetier.getTel());
		personneDao.setAdresse(personneMetier.getAdresse());
		personneDao.setVille(personneMetier.getVille());
		personneDao.setRole(mappingRoleMetierToRoleDao(personneMetier.getRole()));
		personneDao.setAuthentification(mappingAuthentificationMetierToAuthentificationDAO(personneMetier.getAuthentification()));
		
		return personneDao;
	}

	public static RoleDao mappingRoleMetierToRoleDao(Role role) {
		RoleDao roleDao = new RoleDao();
		roleDao.setIdRole(role.getIdRole());
		roleDao.setTitreRole(role.getTitreRole());
		roleDao.setPersonne(new ArrayList<PersonneDao>());
		//roleDao.setPersonne(mappingPersonneMetierToPersonneDao(role.getPersonne().get(index)));
		return roleDao;
	}
	
	public static Personne mappingPersonneDaoToPersonneMetier(PersonneDao personneDao) {
		//Personne personne = new Personne();
		Personne personne = null;
		Authentification authentification = new Authentification();
		authentification.setIdAuthentification(personneDao.getAuthentification().getIdAuthentification());
		authentification.setLogin(personneDao.getAuthentification().getLogin());
		authentification.setMotDePasse(personneDao.getAuthentification().getPassword());
		if(personneDao.getRole().getIdRole() == 1) {
			personne = new Admin();
		}
		else if(personneDao.getRole().getIdRole() == 2) {
			personne = new Volontaire();
		}
		else {
			personne = new Demandeur();
		}
		personne.setId(personneDao.getIdPersonne());
		personne.setNom(personneDao.getNom());
		personne.setPrenom(personneDao.getPrenom());
		personne.setEmail(personneDao.getMail());
		personne.setTel(personneDao.getTel());
		personne.setAdresse((personneDao.getAdresse()));
		personne.setAuthentification(authentification);
		personne.setRole(mappingRoleDaoToRoleMetier(personneDao.getRole()));

		return personne;
	}
	

	public static Authentification mappingAuthentificationDaoToAuthentification(AuthentificationDao authentificationDao) {
		Authentification authentification = new Authentification();
		
		authentification.setIdAuthentification(authentificationDao.getIdAuthentification());
		authentification.setLogin(authentificationDao.getLogin());
		authentification.setMotDePasse(authentificationDao.getPassword());
		//authentification.setPersonne(mappingPersonneDaoToPersonneMetier(authentificationDao.getPersonne()));
		return authentification;
	}

	/*public static Personne getPersonneViaAuthentificationDao(PersonneDao personneDao) {
		Personne personne = null;
		AuthentificationDao authDao = personneDao.getAuthentification();
		
		
		return personne;
	}*/

	

	public static Role mappingRoleDaoToRoleMetier(RoleDao roleDao) {
		Role role = new Role();
		role.setIdRole(roleDao.getIdRole());
		role.setTitreRole(roleDao.getTitreRole());
		//role.setPersonne(mappingListPersonneDaoToListPersonneMetier(roleDao.getPersonne()));
		return role;
	}
	

	public static List<Personne> mappingListPersonneDaoToListPersonneMetier(
			List<PersonneDao> listPersonneDao) {
		List<Personne> listPersonne = new ArrayList<Personne>();

		for (PersonneDao personneDao : listPersonneDao) {
			Personne personne = mappingPersonneDaoToPersonneMetier(personneDao);
			listPersonne.add(personne);
		}

		return listPersonne;
	}
	
	public static List<PersonneDao> mappingListPersonneMetierToListPersonneDao(
			List<Personne> listPersonne) {
		List<PersonneDao> listPersonneDao = new ArrayList<PersonneDao>();

		for (Personne personne : listPersonne) {
			PersonneDao personneDao = mappingPersonneMetierToPersonneDao(personne);
			listPersonneDao.add(personneDao);
		}

		return listPersonneDao;
	}
	

	
	
}
