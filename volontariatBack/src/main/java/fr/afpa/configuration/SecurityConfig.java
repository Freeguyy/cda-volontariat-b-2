//package fr.afpa.configuration;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.security.provisioning.InMemoryUserDetailsManager;
//import org.springframework.security.core.userdetails.User;
//
//@SuppressWarnings("deprecation")
//@Configuration
//@EnableWebSecurity
//@EnableGlobalMethodSecurity(securedEnabled = true, jsr250Enabled = true, prePostEnabled = true)
//public class SecurityConfig extends WebSecurityConfigurerAdapter{
//
//	@Autowired
//	private UserDetailsService userDetailsService;
//	
//	@Autowired
//	public void configureGlobal ( AuthenticationManagerBuilder auth) throws Exception {
//		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
//	}
//	@Bean
//	public UserDetailsService userDetailsService() {
//		InMemoryUserDetailsManager manager= new InMemoryUserDetailsManager();
//		manager.createUser(User.withDefaultPasswordEncoder().username("user").password("user").roles("USER").build());
//				return manager;
//	}
//	
//	//deprecier
////	@Bean
////	public NoOpPasswordEncoder passwordEncoder() {
////		return (NoOpPasswordEncoder) NoOpPasswordEncoder.getInstance();
////	}
//	
//	//duplicate, bizarre l'autowired devrait faire effet ? a verifier
////	@Bean
////	public NoOpPasswordEncoder passwordEncoder() {
////		return (NoOpPasswordEncoder) NoOpPasswordEncoder.getInstance();
////	}
//	
//	//duplicate
//	@Bean
//	public BCryptPasswordEncoder passwordEncoder() {
//		return new BCryptPasswordEncoder();
//	}
//	
//	@Override
//	protected void configure(HttpSecurity http) throws Exception {
//		http.authorizeRequests().antMatchers("/login").permitAll();
//		http.authorizeRequests().anyRequest().authenticated().and().formLogin().loginPage("/login").and().logout().permitAll();
//		http.csrf().disable();
//	}
//}
