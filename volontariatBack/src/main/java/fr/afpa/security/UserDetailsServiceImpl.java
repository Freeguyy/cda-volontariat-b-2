package fr.afpa.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import fr.afpa.dao.repositories.UserRepository;
import fr.afpa.entitedao.UserDao;

@Service
public class UserDetailsServiceImpl implements UserDetailsService{

	@Autowired UserRepository userRepository;
	
	@Override
	public UserDetailsImpl loadUserByUsername(String username) throws UsernameNotFoundException {
		UserDao userDao = userRepository.findByUserDaoName(username);
		if(userDao == null) {
			throw new UsernameNotFoundException("Pas de nom d'user "+username);
		} else {
			return new UserDetailsImpl(userDao);
		}
	}
}
