package fr.afpa.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import fr.afpa.entitedao.RoleDao;
import fr.afpa.entitedao.UserDao;

public class UserDetailsImpl implements UserDetails{

	private UserDao userDao;
	
	public UserDetailsImpl(UserDao userDao) {
		this.userDao = userDao;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		final List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		for ( final RoleDao roleDao : userDao.getRolesDao() )
			authorities.add(new SimpleGrantedAuthority(roleDao.getTitreRole()));
		return authorities;
	}

	@Override
	public String getPassword() {
		
		return userDao.getPassword();
	}

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return userDao.getUserDaoName();
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}
}
