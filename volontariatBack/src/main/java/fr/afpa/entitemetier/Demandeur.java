package fr.afpa.entitemetier;

import java.time.LocalDate;

public class Demandeur extends Personne{
	
	public Demandeur() {
		super();
	}

	public Demandeur(String ville,LocalDate dateDeNaissance, String nom, String prenom,String email, String tel, String cp,Civilite civilite, Authentification authentification, Role role) {
		this.nom=nom;
		this.ville=ville;
		this.prenom=prenom;
		this.email=email;
		this.dateDeNaissance=dateDeNaissance;
		this.tel=tel;
		this.cp=cp;
		this.civilite=civilite;
		this.authentification= authentification;
		this.role=role;
	}
}
