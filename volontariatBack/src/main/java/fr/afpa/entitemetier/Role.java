package fr.afpa.entitemetier;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString

public class Role {

	private Long idRole;
	private String titreRole;
	//private List<Personne> personne;
	
	public Role() {
		super();
	}

	public Role(Long idRole, String titreRole) {
		super();
		this.idRole = idRole;
		this.titreRole = titreRole;
		//this.personne = new ArrayList<Personne>();
	}

	
}
