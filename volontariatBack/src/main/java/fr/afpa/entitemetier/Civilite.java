package fr.afpa.entitemetier;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString

public class Civilite {

	private Long idCivilite;
	private String titreCivilite;
	
	public Civilite() {
		super();
		}
	public Civilite(Long idCivilite, String titreCivilite) {
		super();
		this.idCivilite = idCivilite;
		this.titreCivilite = titreCivilite;
	}
}


