package fr.afpa.entitemetier;

import java.time.LocalDate;

public class Admin extends Personne {

	
	public Admin() {
		super();
	}
	
	public Admin(String ville,LocalDate dateDeNaissance, String nom, String prenom,String email,String cp, String tel,Civilite civilite, Authentification authentification, Role role) {
		this.nom=nom;
		this.ville=ville;
		this.prenom=prenom;
		this.email=email;
		this.tel=tel;
		this.dateDeNaissance=dateDeNaissance;
		this.cp=cp;
		this.civilite=civilite;
		this.authentification= authentification;
		this.role=role;
	}
}
